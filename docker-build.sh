#!/bin/sh
docker build . -t classifieds-cass-api
echo
echo
echo "To run the docker container execute:"
echo "    $ docker run -p 8080:8080 classifieds-cass-api"
