package classifieds.cass.api.controllers;

import classifieds.cass.api.models.ChatMessage;
import classifieds.cass.api.models.ChatConnection;
import classifieds.cass.api.repositories.ChatRepository;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Post;
import io.micronaut.http.annotation.Delete;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.Body;
import io.micronaut.http.HttpStatus;

import javax.inject.Inject;
import java.util.List;

@Controller("/chat")
public class ChatController {

    @Inject
    ChatRepository chatRepository;

    @Post("/connection")
    public HttpStatus createConnection(@Body ChatConnection chatConnection) {
        return HttpStatus.OK;
    }

    @Delete("/connection/{connectionId}")
    public HttpStatus deleteConnection(String connectionId) {
        return HttpStatus.OK;
    }

    @Post("/message")
    public HttpStatus createMessage(@Body ChatMessage chatMessage) {
        return HttpStatus.OK;
    }

    @Get("/conversation/{userId}/{recipientId}")
    public HttpResponse<List<ChatMessage>> getConversation(String userId, String recipientId) {
        List<ChatMessage> chatMessages = chatRepository.getConversation(userId, recipientId);
        return HttpResponse.ok(chatMessages);
    }

}