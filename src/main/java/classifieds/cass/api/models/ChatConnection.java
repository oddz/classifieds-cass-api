package classifieds.cass.api.models;

import com.datastax.driver.mapping.annotations.Column;
import com.datastax.driver.mapping.annotations.PartitionKey;
import io.micronaut.core.annotation.Introspected;
import com.datastax.driver.mapping.annotations.Table;
import java.util.UUID;

@Introspected
@Table(name = "ChatConnections")
public class ChatConnection {

    @PartitionKey
    @Column(name = "Id")
    private UUID id;

    @Column(name = "ConnectionId")
    private String connectionId;

    public ChatConnection setId(UUID id) {
        this.id = id;
        return this;
    }

    public ChatConnection setConnectionId(String connectionId) {
        this.connectionId = connectionId;
        return this;
    }

    public UUID getId() {
        return id;
    }
    public String getConnectionId() {
        return connectionId;
    }

}
