package classifieds.cass.api.models;

import com.datastax.driver.mapping.annotations.Column;
import com.datastax.driver.mapping.annotations.PartitionKey;
import com.datastax.driver.mapping.annotations.Table;
import io.micronaut.core.annotation.Introspected;
import java.util.UUID;

@Introspected
@Table(name = "ChatMessage")
public class ChatMessage {

    @PartitionKey
    @Column(name = "Id")
    private UUID id;

    @Column(name = "SenderId")
    private String senderId;

    @Column(name = "RecipientId")
    private String recipientId;

    @Column(name = "Message")
    private String message;

    public ChatMessage setId(UUID id) {
        this.id = id;
        return this;
    }

    public ChatMessage setSenderId(String senderId) {
        this.senderId = senderId;
        return this;
    }

    public ChatMessage setRecipientId(String recipientId) {
        this.recipientId = recipientId;
        return this;
    }

    public ChatMessage setMessage(String message) {
        this.message = message;
        return this;
    }

    public UUID getId() {
        return id;
    }

    public String getSenderId() {
        return senderId;
    }

    public String getRecipientId() {
        return recipientId;
    }

    public String getMessage() {
        return message;
    }

}
