package classifieds.cass.api.repositories;

import classifieds.cass.api.models.ChatMessage;
import com.datastax.oss.driver.api.core.CqlSession;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Singleton
public class ChatRepository {

    @Inject
    // @Named("default")
    CqlSession session;

    public List<ChatMessage> getConversation(String userId, String recipientId) {
        ChatMessage chatMessage = new ChatMessage() {{
            setId(UUID.randomUUID());
            setSenderId("bbbbb");
            setRecipientId("bbbbb");
            setMessage("This is the message");
        }};
        List<ChatMessage> chatMessages = new ArrayList<ChatMessage>();
        chatMessages.add(chatMessage);
        return chatMessages;
    }
}
